import React from "react";
import './card.styles.css';

const Card = props => (
  <div className="card-container">
		<img src={`https://robohash.org/${props.monster.id}?set=set2&size180x180`} alt="monster" />
    <h2>{props.monster.name}</h2>
  </div>
);

export default Card;
